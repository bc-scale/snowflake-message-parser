# Snowflake docker log message parser (for telegraf)

[![pipeline status](https://gitlab.com/cooling75/snowflake-message-parser/badges/main/pipeline.svg)](https://gitlab.com/cooling75/snowflake-message-parser/-/commits/main)
[![Latest Release](https://gitlab.com/cooling75/snowflake-message-parser/-/badges/release.svg)](https://gitlab.com/cooling75/snowflake-message-parser/-/releases)

# Introduction

Snowflake can be used by people all over the world to mitigate censorship in their countries. More information about the snowflake project can be be found [here](https://snowflake.torproject.org/).  

The easiest way to contribute is to share your internet bandwidth only using your browser with a simple plugin or [by visiting a website](https://cooling75.github.io/relaylove/). There is also the possibility to setup a persistent proxy on a computer/server which is running 24/7, e.g. using docker.

## Background

If you decide to host a standalone proxy server you might would like to see how much traffic flow through your proxy, right? You can use '_docker logs_' (see below) but fetching the logs manually is not really an option for today. Since there are nice tools like telegraf, InfluxDB and Grafana you can build dashboards just with a few clicks. This is where this project comes into play. The stack I am using:

1. docker (datasource)
2. telegraf (docker_log plugin)
3. influxdb (database)
4. Grafana (visualization)

## Docker

[Snowflake docker setup page](https://community.torproject.org/relay/setup/snowflake/standalone/)

From the running docker container you can retrieve the logs with

```bash
docker logs <your snowflake container>
``` 

After a while (1 hour) the logs show messages like:

```bash
2022/11/21 21:11:20 In the last 1h0m0s, there were 1 connections. Traffic Relayed ↑ 2 MB, ↓ 338 KB.
```

## Setup

### TL;DR

Just use docker image [registry.gitlab.com/cooling75/snowflake-message-parser](registry.gitlab.com/cooling75/snowflake-message-parser) for setting up your telegraf container. It additionally contains the compiled __parser__ (main.go) from the repository.

### Telegraf.conf

Your telegraf.conf now can use the parser using the execd plugin:

```telegraf
...
# PROCESSOR PLUGINS

[[processors.execd]]

  command = ["/etc/telegraf/parser"]

# INPUT PLUGINS

[[inputs.docker_log]]
  
  endpoint = "unix:///var/run/docker.sock"
  from_beginning = true
  container_name_include = ["<your snowflake container name>"]
  source_tag = false

  [inputs.docker_log.tagdrop]
    stream = ['stdout']
```