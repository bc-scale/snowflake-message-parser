package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"

	"github.com/influxdata/telegraf/plugins/parsers/influx"
	"github.com/influxdata/telegraf/plugins/serializers"
)

func main() {
	upstreamInt := 0
	downstreamInt := 0
	downstream := 0
	upstream := 0
	cons := 0

	parser := influx.NewStreamParser(os.Stdin)
	serializer, _ := serializers.NewInfluxSerializer()

	for {
		metric, err := parser.Next()
		if err != nil {
			if err == influx.EOF {
				return // stream ended
			}
			if parseErr, isParseError := err.(*influx.ParseError); isParseError {
				fmt.Fprintf(os.Stderr, "parse ERR %v\n", parseErr)
				os.Exit(1)
			}
			fmt.Fprintf(os.Stderr, "ERR %v\n", err)
			os.Exit(1)
		}

		c, found := metric.GetField("message")
		if !found {
			fmt.Fprintf(os.Stderr, "metric has no message field\n")
			os.Exit(1)
		}
		// check if message is a regular log message otherwise skip
		checkre := regexp.MustCompile("^[0-9]{4}/[0-9]{2}/[0-9]{2}.+B.$")
		if !checkre.MatchString(c.(string)) {
			fmt.Fprintf(os.Stderr, "No regular log message! Skipping entry...\n")
			return
		}

		// find upstream
		reCon := regexp.MustCompile("^.*\\W(?P<CONNECTIONS>[0-9]{1,5})\\Wconnections.*$")
		conStr := reCon.FindStringSubmatch(c.(string))

		// find upstream
		regexUpstreamIndicator := regexp.MustCompile("^.*\\W(?P<UPSTREAM>[0-9]{1,4})\\W.B,.*$")
		upstreamStr := regexUpstreamIndicator.FindStringSubmatch(c.(string))

		// find upstream unit
		regexUpstreamUnitIndicator := regexp.MustCompile("^.*\\W(?P<UPSTREAM_UNIT>.B),.*$")
		upstreamUnit := regexUpstreamUnitIndicator.FindStringSubmatch(c.(string))

		// // find downstream
		regexDownstreamIndicator := regexp.MustCompile("^.*\\W(?P<DOWNSTREAM>[0-9]{1,4})\\W.B\\.*$")
		downstreamStr := regexDownstreamIndicator.FindStringSubmatch(c.(string))

		// // find downstream unit
		regexDownstreamUnitIndicator := regexp.MustCompile("^.*\\W(?P<DOWNSTREAM_UNIT>.B)\\..*$")
		downstreamUnit := regexDownstreamUnitIndicator.FindStringSubmatch(c.(string))

		if len(upstreamStr) == 2 {
			upstream, err = strconv.Atoi(upstreamStr[1])
			if err != nil {
				// ... handle error
				fmt.Fprintf(os.Stderr, "upstreamStr: %s\n", upstreamStr)
				panic(err)
			}
		}

		if len(downstreamStr) == 2 {
			downstream, err = strconv.Atoi(downstreamStr[1])
			if err != nil {
				// ... handle error
				fmt.Fprintf(os.Stderr, "downstreamStr: %s\n", downstreamStr)
				panic(err)
			}
		}

		if len(conStr) == 2 {
			cons, err = strconv.Atoi(conStr[1])
			if err != nil {
				// ... handle error
				fmt.Fprintf(os.Stderr, "conStr: %s\n", conStr)
				panic(err)
			}
		}

		// convert mb to kb for both streams
		if len(upstreamUnit) == 2 {
			switch upstreamUnit[1] {
			case "MB":
				upstreamInt = upstream * 1000
			case "KB":
				upstreamInt = upstream
			}
		}

		if len(downstreamUnit) == 2 {
			switch downstreamUnit[1] {
			case "MB":
				downstreamInt = downstream * 1000
			case "KB":
				downstreamInt = downstream
			}
		}

		metric.AddField("up", upstreamInt)
		metric.AddField("down", downstreamInt)
		metric.AddField("cons", cons)
		metric.SetTime(metric.Time())

		// reset variables
		upstreamInt = 0
		downstreamInt = 0
		cons = 0

		b, err := serializer.Serialize(metric)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ERR %v\n", err)
			os.Exit(1)
		}
		fmt.Fprint(os.Stdout, string(b))
	}
}
